#include "Socket.h"


Socket::Socket(const char* address, unsigned short port)
{
    WSAStartup(MAKEWORD(2, 2), &wsaData);
    sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock == INVALID_SOCKET)

    {
        printf("Client: socket() failed! Error code: %ld\n", WSAGetLastError());
        WSACleanup();
        return;
    }
    else
        printf("Client: socket() is OK!\n");

    ServerAddr.sin_family = AF_INET;
    ServerAddr.sin_port = htons(port);
    ServerAddr.sin_addr.s_addr = inet_addr(address);

    int RetCode = connect(sock, (SOCKADDR*)&ServerAddr, sizeof(ServerAddr));

    DWORD timeout =  10;
    setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);

    if (RetCode != 0)
    {
        printf("Client: connect() failed! Error code: %ld\n", WSAGetLastError());
        closesocket(sock);
        WSACleanup();
        return;

    }
    else
    {
        printf("Client: connect() is OK, got connected...\n");
        printf("Client: Ready for sending and/or receiving data...\n");
    }

}


Socket::~Socket()
{
    closesocket(sock);
    WSACleanup();
}

size_t Socket::sendSock(const char* buffer)
{
    return send(sock, buffer, strlen(buffer), 0);
}

size_t Socket::recvSock(char* buffer, size_t len)
{
    size_t numOfBytes = 0;

    try {
        numOfBytes = recv(sock, buffer, len, 0);
    }
    catch (std::exception e)
    {
        printf("exception, %s", e.what());
    }
    return numOfBytes;
}
