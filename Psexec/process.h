#pragma once
#include <stdio.h> 
#include <strsafe.h>
#include <string>
#define BUFSIZE 4096 


HANDLE CreateChildProcess(const char* command);
void WriteToPipe(const char* buff);
std::string ReadFromPipe(void);
void ErrorExit(PTSTR);


HANDLE CreateProcessAndRedirectOutput(const char* command);