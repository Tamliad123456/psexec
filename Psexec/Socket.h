#pragma once
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN
#include <exception>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>

// Need to link with Ws2_32.lib, Mswsock.lib, and Advapi32.lib
#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


class Socket
{
public:
    Socket(const char* address, unsigned short port);
    ~Socket();
    size_t sendSock(const char* buffer);
    size_t recvSock(char* buffer, size_t len);


private:
    WSADATA  wsaData;
    SOCKET sock;
    SOCKADDR_IN ServerAddr, ThisSenderInfo;
};

