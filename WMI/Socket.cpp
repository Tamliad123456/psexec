#include "Socket.h"


Socket::Socket(unsigned short port)
{
	struct sockaddr_in server;

	WSAStartup(MAKEWORD(2, 2), &wsaData);
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
	}

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(port);

	//Bind
	if (bind(sock, (struct sockaddr*)&server, sizeof(server)) == SOCKET_ERROR)
	{
		printf("Bind failed with error code : %d", WSAGetLastError());
	}
	else
		wprintf(L"bind returned success\n");
	listen(sock, 1);

	DWORD timeout = 10;

	clientSock = accept(sock, NULL, NULL);
	setsockopt(clientSock, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeout, sizeof timeout);

	if (clientSock == INVALID_SOCKET)
	{
		printf("socker connection error");
		WSACleanup();
		return;
	}


}


Socket::~Socket()
{
	closesocket(sock);
	try 
	{

		closesocket(clientSock);
	}
	catch (std::exception e)
	{
		printf("exception: %s", e.what());
	}
	WSACleanup();
}

size_t Socket::sendSock(const char* buffer)
{
	return send(clientSock, buffer, strlen(buffer), 0);
}

size_t Socket::recvSock(char* buffer, size_t len)
{
	size_t numOfBytes = 0;
	
	numOfBytes = recv(clientSock, buffer, len, 0);
	return numOfBytes;
}
